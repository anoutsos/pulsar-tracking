import sys
import matplotlib.pyplot as plt
import numpy as np
import glob
import pylab
import math
import matplotlib.transforms as transforms
from scipy import constants
from scipy import interpolate
from scipy import signal

plt.rcParams["figure.figsize"] = (15,10)
plt.rcParams.update({'font.size': 10})
#ax = plt.gca()

nsubs = 2

f, axarr = plt.subplots(nsubs,1, sharex=True, figsize=(15,10))

f.subplots_adjust(hspace=0.1)





Ts = 86400.0
phi = -30.7 * np.pi/180.0
hdot = 2.0 * np.pi/Ts

alpha = 0.0 * np.pi/12.0
delta = (phi-5.0) * np.pi/180.0


D = 20000.0


tlim = 43200

tarr = np.linspace(-tlim, tlim, int(2*abs(tlim)))
tarrh = np.linspace(-tlim/3600, tlim/3600, int(2*abs(tlim)))


def l0(t,th):
  return D*math.sin(t*2.0*np.pi/Ts)

def l1(t,th):
  return D*math.sin(t*2.0*np.pi/Ts+th)

def d1(lam):
	if(lam>300.0/350.0):
	 	return 38.0
	else:
		return 15.0

def hwhm1(lam):
	return 0.6*lam/d1(lam)

def tang(arr,fac):
	win = int(len(arr)/fac)
	narr = arr[0::win]
	darr = np.diff(arr)[0::win]
	return np.array([narr[i]+darr[i]*range(win) for i in range(len(narr))]).flatten()

factor = 864
wavl = 6.0
nb = 1.0

l0arr = np.array([l0(t,nb*hwhm1(wavl)) for t in tarr])
l1arr = np.array([l1(t,nb*hwhm1(wavl)) for t in tarr])

t0arr = 1e6*l0arr/constants.c
t1arr = 1e6*l1arr/constants.c

dlarr = l1arr-l0arr
dtarr = t1arr-t0arr

res0arr = t0arr-tang(t0arr,factor)
res1arr = t1arr-tang(t1arr,factor)
resdtarr = dtarr-tang(dtarr,factor)


axarr[1].set_xlabel('HA (hours)')

axarr[0].set_ylabel(r'$t_{\rm g}\ (\mu{\rm s})$')
axarr[1].set_ylabel(r'$\Delta t_{\rm g}\ ({\rm ps})$')

axarr[0].plot(tarrh,tang(dtarr,factor),color='red')

axarr[1].plot(tarrh,1e6*resdtarr,color='black')

axarr[1].axhline(y=0, color='gray', linestyle='--')

HAlimit = 45.0
label = str(HAlimit)+r'$^\circ$'
HAlimit = HAlimit * 24.0/360.0

for i in range(nsubs):
	axarr[i].axvline(x = -HAlimit, color = 'gray', linestyle='--',label = r'-45^\circ')
	axarr[i].axvline(x = HAlimit, color = 'gray', linestyle='--', label = r'45^\circ')
	if i==0:
		axarr[i].annotate(r'$-$'+label,(-HAlimit,0),textcoords="offset points", xytext=(10,-50),ha='center',rotation=90)
		axarr[i].annotate(label,(HAlimit,0),textcoords="offset points", xytext=(10,50),ha='center',rotation=90)


# Show the plot
plt.show()

