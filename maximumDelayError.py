import sys
import matplotlib.pyplot as plt
import numpy as np
import glob
import pylab
import math
import matplotlib.transforms as transforms
from scipy import constants
from scipy import interpolate
from scipy import signal

plt.rcParams["figure.figsize"] = (15,10)
plt.rcParams.update({'font.size': 10})
#ax = plt.gca()

nsubs = 1

f, axarr = plt.subplots(nsubs,1, sharex=True, figsize=(15,10))

f.subplots_adjust(hspace=0.1)




Ts = 86400.0
phi = -30.7 * np.pi/180.0
hdot = 2.0 * np.pi/Ts

alpha = 0.0 * np.pi/12.0
delta = (phi-5.0) * np.pi/180.0


D = 20000.0


tlim = 43200

tarr = np.linspace(-tlim, tlim, int(2*abs(tlim)))
tarrh = np.linspace(-tlim/3600, tlim/3600, int(2*abs(tlim)))


def l0(t,th):
  return D*math.sin(t*2.0*np.pi/Ts)

def l1(t,th):
  return D*math.sin(t*2.0*np.pi/Ts+th)

def d1(lam):
	if(lam>300.0/350.0):
	 	return 38.0
	else:
		return 15.0

def hwhm1(lam):
	return 0.6*lam/d1(lam)

def tang(arr,fac):
	win = int(len(arr)/fac)
	narr = arr[0::win]
	darr = np.diff(arr)[0::win]
	return np.array([narr[i]+darr[i]*range(win) for i in range(len(narr))]).flatten()

factor = 17280
wavl = 6.0
nb = 3.0

for nb in [1,2,4]:
	l0arr = np.array([l0(t,nb*hwhm1(wavl)) for t in tarr])
	l1arr = np.array([l1(t,nb*hwhm1(wavl)) for t in tarr])

	t0arr = 1e6*l0arr/constants.c
	t1arr = 1e6*l1arr/constants.c

	dlarr = l1arr-l0arr
	dtarr = t1arr-t0arr

	res0arr = t0arr-tang(t0arr,factor)
	res1arr = t1arr-tang(t1arr,factor)
	resdtarr = dtarr-tang(dtarr,factor)

	facs = [108.,216.,432.,864.,1080.,1728.,2160.,4320.,8640.,17280.]
	tfacs = 2.0*tlim*np.reciprocal(facs)
	maxresdtarr = [1e6*max(abs(dtarr-tang(dtarr,fc))) for fc in facs]
	fint = interpolate.interp1d(tfacs, maxresdtarr,kind='quadratic')
	tfacs_int = np.arange(np.min(tfacs), np.max(tfacs), 0.5)
	maxresdtarr_int = fint(tfacs_int)

	value=119
	xvalue = tfacs_int[(np.abs(maxresdtarr_int-value)).argmin()]

	dphiarr = np.round(dlarr/wavl)-dlarr/wavl

	axarr.set_yscale('log')

	axarr.set_xlabel(r'$\tau_{\rm d}\ ({\rm s})$')

	axarr.set_ylabel(r'$\Delta t_{\rm g}\ ({\rm ps})$')

	axarr.plot(tfacs_int,maxresdtarr_int,color=plt.cm.gray(nb/5))

	axarr.axhline(y=value, color='gray', linestyle='--')

	axarr.vlines(x=xvalue, ymin=0, ymax=value, color=plt.cm.gray(nb/5), linestyle='-', zorder=2)

	if(nb==1):
		axarr.annotate(r'$\theta_{\rm hwhm}$',(max(tfacs_int),max(maxresdtarr_int)),textcoords="offset points", xytext=(20,-3),ha='center')
	else:
		axarr.annotate(str(nb)+r'$\theta_{\rm hwhm}$',(max(tfacs_int),max(maxresdtarr_int)),textcoords="offset points", xytext=(20,-3),ha='center')

	axarr.annotate(str("{:.0f}".format(xvalue)),(xvalue,0.2),textcoords="offset points", xytext=(0,-34),ha='center', color=plt.cm.Reds(1-nb/5), rotation=0)
	
	axarr.annotate(str("{:.0f}".format(value)+' ps'),(0,value),textcoords="offset points", xytext=(-10,5),ha='center', color='gray', rotation=0)



# Show the plot
plt.show()

