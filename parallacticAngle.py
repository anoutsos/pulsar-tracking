import sys
import matplotlib.pyplot as plt
import numpy as np
import glob
import pylab
import math
import matplotlib.transforms as transforms

plt.rcParams["figure.figsize"] = (15,10)
plt.rcParams.update({'font.size': 10})
#ax = plt.gca()

f, axarr = plt.subplots(3,2, sharex=True, figsize=(15,10))

f.subplots_adjust(hspace=0.1)



Ts = 86400.0
phi = -30.7 * np.pi/180.0
hdot = 2.0 * np.pi/Ts

alpha = 0.0 * np.pi/12.0
delta = (phi-5.0) * np.pi/180.0


d = 13.5

tlim = -18000

tarr = np.linspace(-tlim, tlim, int(2*abs(tlim)))
# baslarr = np.linspace(500, 20000, 5)
# wavlarr = np.linspace(0.04, 3, 5)

tarr2 = np.linspace(-tlim/3600, tlim/3600, int(2*abs(tlim)))


baslarr = np.array([20000])
wavlarr = np.array([0.019])
# wavlarr = np.array([3.0])


def q(t,dlt):
  return math.atan2(math.sin(hdot*t),math.cos(dlt)*math.tan(phi)-math.sin(dlt)*math.cos(hdot*t))


def sinz(t,dlt):
  return (np.cos(dlt)*np.sin(phi)-np.sin(dlt)*np.cos(phi)*np.cos(hdot*t))/np.cos(q(t,dlt))

def qdot(t,dlt):
  return np.cos(phi)*(np.cos(hdot*t)*np.cos(dlt)*np.sin(phi)-np.sin(dlt)*np.cos(phi))/pow(sinz(t,dlt),2) * hdot


def d1(lam):
	if(lam>300.0/350.0):
	 	return 38.0
	else:
		return 15.0

def fwhm1(lam):
	return 1.02*lam/d1(lam)

def fwhmTA(lam,D):
	return 1.02*lam/D


qarr = np.array([180/np.pi * q(t,delta) for t in tarr])

qdotarr = np.array([180/np.pi * abs(qdot(t,delta)) for t in tarr])

# qarruw = np.unwrap(np.array([180.0/np.pi * q(t,delta) for t in tarr]), period=2.0*np.pi)
# qdotuwarr = np.hstack((0, abs(np.diff(qarruw))))

tdrift = np.array([[[0.5*fwhmTA(wavl,basl)/(fwhm1(wavl)*abs(qdot(t,delta))) for t in tarr] for wavl in wavlarr] for basl in baslarr])

wavldrift = np.array([min(tdrift[0][w].flatten()) for w in range(len(wavlarr))])


colors = plt.cm.gray(np.linspace(0,1,len(tdrift)*len(tdrift[0])))

# axarr[1].set_yscale('log')
# axarr[2][1].set_xscale('log')
axarr[2][0].set_ylim([0, 50])

axarr[2][0].set_xlabel('HA (hours)')
axarr[0][0].set_ylabel('PA (degrees)')
axarr[1][0].set_ylabel('PA rate (arcsec/second)')
axarr[2][0].set_ylabel('Drift time (seconds)')


axarr[2][1].set_xlabel('HA (hours)')

# Create the plots
axarr[0][0].plot(tarr2,qarr,color='black')
# axarr[0][1].plot(tarr2,qarruw,color='black')
axarr[1][0].plot(tarr2,qdotarr*3600,color='black')
# axarr[1][1].plot(tarr2,qdotuwarr*3600,color='black')
for i in range(len(tdrift)):
	for j in range(len(tdrift[0])):
		axarr[2][0].plot(tarr2,tdrift[i][j],color=colors[i+len(tdrift)*j])
		# axarr[2].plot(tarr2,tdrift2[i][j],color=colors[i+len(tdrift2)*j])

# axarr[2][1].plot(300.0/wavlarr,wavldrift,color='black')

# axarr[1][0].axhline(y=max(qdotarr*3600), color='r', linestyle='--')
# trans = transforms.blended_transform_factory(axarr[1][0].get_yticklabels()[0].get_transform(), axarr[1][0].transData)
# axarr[1][0].text(0,max(qdotarr*3600), "{:.1f}".format(max(qdotarr*3600)), color="red", transform=trans,ha="right", va="center")

axarr[2][0].axhline(y=min(tdrift.flatten()), color='r', linestyle='--')
trans = transforms.blended_transform_factory(axarr[2][0].get_yticklabels()[0].get_transform(), axarr[2][0].transData)
axarr[2][0].text(0,min(tdrift.flatten()), "{:.1f}".format(min(tdrift.flatten())), color="red", transform=trans,ha="right", va="center")



# Show the plot
plt.show()

