import sys
import matplotlib.pyplot as plt
import numpy as np
import glob
import pylab
import math
import matplotlib.transforms as transforms
from scipy import constants
from scipy import interpolate
from scipy import signal

plt.rcParams["figure.figsize"] = (15,10)
plt.rcParams.update({'font.size': 10})
#ax = plt.gca()

nsubs = 1

f, axarr = plt.subplots(nsubs,1, sharex=True, figsize=(15,10))

f.subplots_adjust(hspace=0.1)

nstat = 512

phierrs = np.linspace(0.0,25.0,2500)
amps = [np.exp(1j*((np.random.rand(1,nstat)-0.5)*2*phierr*np.pi/180)).flatten() for phierr in phierrs]


dBloss = -20.0*np.log10(np.abs(np.sum(amps,1))/len(amps[0]))

axarr.set_ylabel('Gain loss (dB)')
axarr.set_xlabel('Random phase error for individual stations (deg)')

axarr.plot(phierrs,dBloss,color='k')

axarr.axhline(y=0.1, color='r', linestyle='--')

# Show the plot
plt.show()
